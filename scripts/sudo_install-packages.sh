#!/usr/bin/env bash

export INSTALL=$(cat ../packages-to-install |tr '\n' ' ')
echo "Installing the good stuff..."
dnf install --allowerasing ${INSTALL}
