# .bashrc
touch $HOME/.bashrc
export BASHFILE="$HOME/.bashrc"

function add {
    LINE=$1
    COUNT=$(grep "$LINE" $BASHFILE | wc -l)
    if [ "$COUNT" -eq "0" ]; then
	echo $LINE >> $BASHFILE
    else
	echo "Found $COUNT lines in $BASHFILE for: $LINE"
    fi
}

add "source ~/fedora-base/bashsource"
add ". ~/fedora-base/bashalias"
add "source_client"
