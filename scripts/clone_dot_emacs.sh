#!/bin/bash
if [ ! -f ~/.ssh/id_rsa ];
then
    echo "~/.ssh/id_rsa SSH private key not found. Cloning with HTTPS"
    git clone https://gitlab.com/JannuMies/dot_emacs.git ~/dot_emacs
else
    git clone git@gitlab.com:JannuMies/dot_emacs.git ~/dot_emacs
fi

ln -s ~/dot_emacs/.emacs ~/.emacs
