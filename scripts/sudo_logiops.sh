#!/usr/bin/env bash

CONF_LOCATION=/files/logid.cfg
SOURCE_DIR=$(dirname "$0")

SOURCE=$SOURCE_DIR$CONF_LOCATION
TARGET=/etc/logid.cfg

echo $SOURCE
cp $SOURCE $TARGET

systemctl enable logid.service
systemctl start logid.service
