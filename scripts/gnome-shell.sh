#!/bin/bash
# We are cool, no cruise control needed.
gsettings set org.gnome.desktop.input-sources xkb-options ["'caps:ctrl_modifier'"]

# Date and Week in calendar
gsettings set org.gnome.desktop.interface clock-show-weekday false
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.calendar show-weekdate true

# Don't bloody bong me all the time
gsettings set org.gnome.desktop.sound event-sounds false

# Folders before files, we're not part of the bloody fingerpainting idiots brigade
gsettings set org.gtk.Settings.FileChooser sort-directories-first true

# Try to eliminate beeping
gsettings set org.gnome.desktop.sound event-sounds false
gsettings set org.gnome.desktop.sound input-feedback-sounds false

# Scroll like a scroll wheel
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false

# alt-tab is not deranged
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]"

# alt-f5 is alternative binding to unmaximize, re-set unmaximize to only super-down to disable.
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Super>Down']"

# don't idle dim when inactive
gsettings set org.gnome.settings-daemon.plugins.power idle-dim false

# Tap-to-click is my way of doing shit
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
