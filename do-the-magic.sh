#!/usr/bin/env bash

echo "Tweaking Gnome Shell"
bash scripts/gnome-shell.sh

# Basic stuff for kinda everything
sudo dnf -y install dnf-plugins-core

echo "Enabling additional repositories"
echo "  * RPM Fusion"
sudo bash scripts/sudo_rpm_fusion.sh
echo "  * SBT"
sudo bash scripts/sudo_sbt_repo.sh

# We need Emacs and Git
echo "Installing essentials..."
sudo dnf install -y git emacs
bash scripts/clone_dot_emacs.sh 
echo "It's dangerout to go alone. Take the .emacs."
if [ ! -f ~/.emacs ]; then
    echo "~/.emacs does not exist. Aborting!"
    exit 1
fi

echo "Updating system..."
sudo dnf update -y
